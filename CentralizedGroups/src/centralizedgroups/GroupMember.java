/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package centralizedgroups;

import java.io.Serializable;

/**
 *
 * @author usuario
 */
public class GroupMember implements Serializable {
    String alias;
    String hostname;
    int idUser;
    int idGroup;
    
    public GroupMember(String alias, String hostname, int idUser, int idGroup) {
        this.alias = alias;
        this.hostname = hostname;
        this.idUser = idUser;
        this.idGroup = idGroup;
    }
}
