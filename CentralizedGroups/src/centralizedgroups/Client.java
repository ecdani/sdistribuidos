/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package centralizedgroups;

import com.sun.corba.se.spi.activation.Server;
import java.rmi.AccessException;
import java.rmi.NoSuchObjectException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dani
 */
public class Client extends UnicastRemoteObject implements ClientInterface {

    int id; // Identificador del grupo
    GroupServerInterface srv;
    String alias = "", galias = "", ualias = "", hostname = "";
    public Scanner in;

    public Client() throws RemoteException {
        super();
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        System.out.println("Introduce el alias del cliente:");

        in = new Scanner(System.in);
        alias = in.next();
    }

    public void crearGrupo() {
        try {
            System.out.println("Pon nombre de grupo");
            galias = in.next();
            id = srv.createGroup(galias, alias, "localhost");
            if (id != -1) {
                System.out.println("Hemos creado tu grupo, de id: " + id);
            }
        } catch (RemoteException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void EliminarGrupo() {
        try {
            System.out.println("Pon nombre del dueño");
            ualias = in.next();
            System.out.println("Borras por nombre(a) o por id(b)?");
            String op = in.next();
            switch (op) {
                case "a":
                    System.out.println("Pon nombre del grupo");
                    galias = in.next();
                    if (srv.removeGroup(galias, ualias)) {
                        System.out.println("Todo bien");
                    }
                    break;
                case "b":
                    System.out.println("Pon id del grupo");
                    id = in.nextInt();
                    if (srv.removeGroup(id, ualias)) {
                        System.out.println("Todo bien");
                    }
                    break;

            }
        } catch (RemoteException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void AñadirMiembro() {
        try {
            System.out.println("Pon nombre de usuario");
            ualias = in.next();
            System.out.println("Pon nombre de host");
            hostname = in.next();
            System.out.println("Pon id de grupo");
            id = in.nextInt();
            if (srv.addMember(id, ualias, hostname) != null) {
                System.out.println("Todo bien");
            } else {
                System.out.println("Ya existia");
            }
        } catch (RemoteException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void EliminarMiembro() {
        try {
            System.out.println("Pon nombre de usuario");
            ualias = in.next();
            System.out.println("Pon id de grupo");
            id = in.nextInt();
            if (!srv.removeMember(id, ualias)) {
                System.out.println("Ya existía");
            } else {
                System.out.println("Todo bien");
            }
        } catch (RemoteException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void BloquearAltasBajas() {
        try {
            System.out.println("Pon id de grupo");
            id = in.nextInt();
            srv.StopMembers(id);
        } catch (RemoteException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void DesbloquearAltasBajas() {
        try {
            System.out.println("Pon id de grupo");
            id = in.nextInt();
            srv.AllowMembers(id);
        } catch (RemoteException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void Terminar() {
        try {
            UnicastRemoteObject.unexportObject(this, true);
        } catch (NoSuchObjectException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void ComprobarNGrupo() {
        try {
            System.out.println(srv.numeroGrupos());
        } catch (RemoteException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void ComprobarNmiembros() {
        try {
            System.out.println("Alias de grupo");
            System.out.println(srv.numeromiembros(in.next()));
        } catch (RemoteException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @param args the command line arguments
     * @author Dani
     */
    public static void main(String[] args) {
        System.setProperty("java.security.policy", "D:\\Documentos\\Git\\sdistribuidos\\CentralizedGroups\\src\\centralizedgroups\\cliente-policy");

        try {
            Client c = new Client();
            Registry registry = LocateRegistry.getRegistry("localhost", 1099);
            c.srv = (GroupServerInterface) registry.lookup("GroupServer");
            int i = 0;

            System.out.println("Elija una accion:");
            while (i < 10) {
                System.out.println("1. Crear grupo");
                System.out.println("2. Eliminar grupo");
                System.out.println("3. Añadir miembro");
                System.out.println("4. Eliminar miembro");
                System.out.println("5. Bloquear altas/bajas");
                System.out.println("6. Desbloquear altas/bajas");
                System.out.println("7. Terminar");
                System.out.println("8. Comprobar número de grupo");
                System.out.println("9. Comprobar número de miembros");

                // in = new Scanner(System.in);
                String s = c.in.next();
                switch (s) {
                    case "1"://1. Crear grupo
                        c.crearGrupo();
                        break;
                    case "2"://2. Eliminar grupo
                        c.EliminarGrupo();
                        break;
                    case "3"://3. Añadir miembro
                        c.AñadirMiembro();
                        break;
                    case "4"://4. Eliminar miembro
                        c.EliminarMiembro();
                        break;
                    case "5"://5. Bloquear altas/bajas
                        c.BloquearAltasBajas();
                        break;
                    case "6": //6. Desbloquear altas/bajas
                        c.DesbloquearAltasBajas();
                        break;
                    case "7":
                        c.Terminar();
                        i = 10;
                        break;
                    case "8"://8. Comprobar número de grupo
                        c.ComprobarNGrupo();
                        break;
                    case "9":
                        c.ComprobarNmiembros();
                        break;
                }
                i++;
            }
        } catch (RemoteException ex) {
            Logger.getLogger(GroupServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NotBoundException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
