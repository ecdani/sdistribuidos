/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package centralizedgroups;

import java.rmi.AccessException;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario
 */
public class GroupServer extends UnicastRemoteObject implements GroupServerInterface {

    LinkedList<ObjectGroup> objetos = new LinkedList<ObjectGroup>();
    ReentrantLock lock = new ReentrantLock(true);
    int idGroup = 0;

    public GroupServer() throws RemoteException {
        super();
        lock.lock();
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        lock.unlock();

    }

    @Override
    public int createGroup(String galias, String oalias, String ohostname) {
        lock.lock();
        System.out.println("Creando grupo "+galias+"...");
        if (this.findGroup(galias) == -1) {
            ObjectGroup grupo = new ObjectGroup(++idGroup, galias, oalias, ohostname);
            objetos.add(grupo);
            lock.unlock();
        }
        System.out.print("Creado.");
        return idGroup;
    }

    @Override
    public int findGroup(String galias) {
        System.out.println("Buscando grupo "+galias+"...");
        for (int i = 0; i < objetos.size(); i++) {
            if (objetos.get(i).alias.equals(galias)) {
                System.out.print("Encontrado grupo "+galias+".");
                return objetos.get(i).getGid();
            }
        }
        System.out.println("Grupo "+galias+" no encontrado");
        return -1;
    }

    @Override
    public boolean removeGroup(String galias, String oalias) {
        lock.lock();
        System.out.println("Eliminado grupo "+galias+"...");
        int gdelete = this.findGroup(galias);
        if (gdelete != -1) {
            for (int i = 0; i < objetos.size(); i++) {
                if (objetos.get(i).getGid() == gdelete && objetos.get(i).getPropietario().alias.equals(oalias)) {
                    objetos.remove(i);
                    System.out.print(" Grupo "+galias+" eliminado.");
                    lock.unlock();
                    return true;
                }
            }
            System.out.println("Existe pero el dueño no es ese.");
        }
        return false;
    }

    @Override
    public boolean removeGroup(int gid, String oalias) {
        lock.lock();
        System.out.println("Eliminado grupo "+gid+"...");
        if (gid != -1 && gid <= idGroup) {
            for (int i = 0; i < objetos.size(); i++) {
                if (objetos.get(i).getGid() == gid && objetos.get(i).getPropietario().alias.equals(oalias)) {
                    objetos.remove(i);
                    System.out.print("grupo "+gid+" eliminado.");
                    lock.unlock();
                    return true;
                }
            }

        }
        return false;
    }

    @Override
    public GroupMember addMember(int gid, String alias, String hostname) {
        lock.lock();
        System.out.print("Añadiendo miembro "+alias+"...");
        for (int i = 0; i < objetos.size(); i++) {
            if (objetos.get(i).getGid() == gid && objetos.get(i).isMember(alias) == null) {
                lock.unlock();
                
                return objetos.get(i).addMember(alias, hostname);
                
            }
        }
        return null;
    }

    /**
     * Eliminamos al usuario con alias "alias" que pertenece al grupo con id
     * "gid". En principio no se puede borrar al propietario, si hay tiempo
     * hacemos una version elaborada.
     *
     * @param gid
     * @param alias
     * @return
     */
    @Override
    public boolean removeMember(int gid, String alias) {
        lock.lock();
        System.out.println("Eliminado miembro "+alias+"...");
        if (gid != -1 && gid <= idGroup) {
            for (int i = 0; i < objetos.size(); i++) {
                if (objetos.get(i).getGid() == gid) {
                    System.out.print("Eliminado.");
                    lock.unlock();
                    return objetos.get(i).removeMember(alias);
                }
            }

        }
        return false;
    }

    @Override
    public GroupMember isMember(int gid, String alias) {
        lock.lock();
        System.out.println("Comprobando pertenencia miembro "+alias+" a grupo "+gid+"...");
        if (gid != -1 && gid <= idGroup) {
            for (int i = 0; i < objetos.size(); i++) {
                if (objetos.get(i).getGid() == gid) {
                    System.out.print("Es miembro.");
                    lock.unlock();
                    return objetos.get(i).isMember(alias);
                }
            }
        }
        return null;
    }

    @Override
    public boolean StopMembers(int gid) {
        lock.lock();
        System.out.println("Bloqueando altas y bajas de miembros al grupo "+gid+"...");
        if (gid != -1 && gid <= idGroup) {
            for (int i = 0; i < objetos.size(); i++) {
                if (objetos.get(i).getGid() == gid) {
                    lock.unlock();
                    objetos.get(i).StopMembers();
                    System.out.print("Bloqueadas.");
                    return true;
                }
            }

        }
        return false;
    }

    @Override
    public boolean AllowMembers(int gid) {
        lock.lock();
         System.out.println("Desbloqueando altas y bajas de miembros al grupo "+gid+"...");
        for (int i = 0; i < objetos.size(); i++) {
            if (objetos.get(i).getGid() == gid) {
                lock.unlock();
               objetos.get(i).AllowMembers();
               System.out.print("Desbloqueadas.");
               return true;
            }
        }
        return false;
    }

    public int numeroGrupos() {
        return objetos.size();
    }
    
    public int numeromiembros(String alias) {
         for (int i = 0; i < objetos.size(); i++) {
             //System.out.println("Numero miembros, iteracion"+i+" Alias explorado:"+objetos.get(i).getAlias()+" Alias buscado:"+alias);
                if (objetos.get(i).getAlias().equals(alias)) {
                    return objetos.get(i).miembros.size();
                }
            }
         return -1;
    }
    /**
     * @param args the command line arguments
     * @author Dani
     */
    public static void main(String[] args) {
        System.setProperty("java.security.policy", "D:\\Documentos\\Git\\sdistribuidos\\CentralizedGroups\\src\\centralizedgroups\\server-policy");
        try {
            GroupServer srv = new GroupServer();
            LocateRegistry.createRegistry(1099);
            Registry registry = LocateRegistry.getRegistry(1099);

            registry.rebind("GroupServer", srv);

        } catch (RemoteException ex) {
            Logger.getLogger(GroupServer.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
