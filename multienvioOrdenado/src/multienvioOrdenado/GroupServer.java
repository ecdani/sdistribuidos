package multienvioOrdenado;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GroupServer extends UnicastRemoteObject implements GroupServerInterface {

    LinkedList<ObjectGroup> objetosGrupo = new LinkedList<>();
    ObjectGroup grupo;
    ReentrantLock lock = new ReentrantLock(true);
    int idGroup = 0;

    public GroupServer() throws RemoteException {
        super();
        lock.lock();
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        lock.unlock();
    }

    @Override
    public int createGroup(String galias, String oalias, String ohostname, int puertoreg) {
        lock.lock();
        System.out.println("Creando grupo "+galias+"...");
        if (this.findGroup(galias) == -1) {
            grupo = new ObjectGroup(++idGroup, galias, oalias, ohostname,puertoreg);
            objetosGrupo.add(grupo);
            lock.unlock();
        }
        System.out.print("Creado.");
        return idGroup;
    }

    /**
     * Encontrar identificador de grupo por alias
     * @param galias Alias de grupo
     * @return int Identificador, -1 si no existe.
     */
    @Override
    public int findGroup(String galias) {
        lock.lock();
        System.out.println("Buscando grupo "+galias+"...");
        try{
        for (ObjectGroup grupo : objetosGrupo) {
            if (grupo.alias.equals(galias)) {
                System.out.print("Encontrado grupo "+galias+".");
                return grupo.getGid();
            }
        }
        System.out.println("Grupo "+galias+" no encontrado");
        return -1;
        } finally {
           lock.unlock();
        }
    }
    
    /**
     * Auxiliar para obtener un grupo por id
     * @param gid Identificador de grupo
     * @return
     */
    public ObjectGroup getGroup(int gid) {
        System.out.println("Buscando grupo " + gid + "...");
        if (gid != -1 && gid <= idGroup) {
            for (ObjectGroup og : objetosGrupo) {
                if (og.gid == gid) {
                    System.out.print("Encontrado grupo " + gid + ".");
                    return og;
                }
            }
        }
        try {
            throw new Exception();
        } catch (Exception e) {
            System.out.println("Grupo " + gid + " no encontrado/ No existe");
        }
        return null;
    }
    
    /**
     * Borrar grupo por alias del grupo
     * @param galias Alias del grupo
     * @param oalias Owner Alias, alias del propietario
     * @return True si se borra, falso si no.
     */
    @Override
    public boolean removeGroup(String galias, String oalias) {
        lock.lock();
        System.out.println("Eliminado grupo "+galias+"...");
        int gdelete = this.findGroup(galias);
        System.out.println("Encontrada correspondencia grupo "+galias+" con identificador "+gdelete+". Borrando...");
        return removeGroup(gdelete,oalias);
    }
    
    /**
     * Borrar grupo por identificador
     * @param gid Identificador del grupo
     * @param oalias Owner Alias, alias del propietario
     * @return True si se borra, falso si no.
     */
    @Override
    public boolean removeGroup(int gid, String oalias) {
        lock.lock();
        System.out.println("Eliminado grupo " + gid + "...");
        grupo = getGroup(gid);
        if (grupo.getPropietario().getAlias().equals(oalias)) {
            objetosGrupo.remove(grupo);
            System.out.print("Grupo " + gid + " eliminado.");
            lock.unlock();
            return true;
        }
        System.out.println("No eres el dueño del grupo. Permiso denegado.");
        return false;
    }

    /**
     * Añadir miembro a un grupo
     * @param gid Identificador de grupo
     * @param alias Alias del miembro
     * @param hostname Nombre de host del cliente
     * @param puertoreg Puerto del cliente
     * @return Objeto GroupMember creado
     */
    @Override
    public GroupMember addMember(int gid, String alias, String hostname, int puertoreg) {
        lock.lock();
        System.out.print("Añadiendo miembro " + alias + "...");
        grupo = getGroup(gid);
        if (grupo.isMember(alias) == null) {
            lock.unlock();
            return grupo.addMember(alias, hostname, puertoreg);
        }
        return null;
    }

    /**
     * Eliminamos al usuario con alias "alias" que pertenece al grupo con id
     * "gid". En principio no se puede borrar al propietario, si hay tiempo
     * hacemos una version elaborada.
     *
     * @param gid
     * @param alias
     * @return
     */
    @Override
    public boolean removeMember(int gid, String alias) {
        lock.lock();
        System.out.println("Eliminado miembro "+alias+"...");
        grupo = getGroup(gid);
        lock.unlock();
        return grupo.removeMember(alias);
    }

    @Override
    public GroupMember isMember(int gid, String alias) {
        lock.lock();
        System.out.println("Comprobando pertenencia miembro " + alias + " a grupo " + gid + "...");
        grupo = getGroup(gid);
        lock.unlock();
        return grupo.isMember(alias);
    }
    /*
    @Override
    public boolean StopMembers(int gid) {
        lock.lock();
        System.out.println("Bloqueando altas y bajas de miembros al grupo " + gid + "...");
        grupo = getGroup(gid);
        lock.unlock();
        grupo.StopMembers();
        System.out.print("Bloqueadas.");
        return true;
    }*/
    /*
    @Override
    public boolean AllowMembers(int gid) {
        lock.lock();
        System.out.println("Desbloqueando altas y bajas de miembros al grupo " + gid + "...");
        grupo = getGroup(gid);
        lock.unlock();
        grupo.AllowMembers();
        System.out.print("Desbloqueadas.");
        return true;
    }*/
    
    /**
     * Auxiliar, no está en la practica
     * @return 
     */
    public int numeroGrupos() {
        return objetosGrupo.size();
    }
    
    /**
     * Auxiliar, no está en la práctica
     * @param alias
     * @return 
     */
    public int numeromiembros(String alias) {
        grupo = getGroup(findGroup(alias));
        return grupo.miembros.size();
    }
    
    /**
     * Callback para que los clientes puedan enviar mensajes a través del
     * servidor a su grupo.
     * @param gm Miembro que envia el mensaje
     * @param msg Mensaje
     * @return True cuando termina el envio.
     */
    @Override
    public boolean sendGroupMessage(GroupMember gm, byte[] msg) {
        System.out.println("Recibida llamada del cliente");
        grupo.Sending();
        lock.lock();
        System.out.println("Inicio envio");
        grupo = getGroup(gm.getIdGroup());
        GroupMessage m = new GroupMessage(msg, gm, grupo.nextSeqNumber(gm.getIdUser()));
        System.out.println("Mensaje de "+gm.getAlias()+" con num de secuencia "+m.getSeqnum());
        SendingMessage envio = new SendingMessage(grupo, m, gm);
        envio.start();
        lock.unlock();
        return true;
    }
    
    /**
     * Main del servidor
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.setProperty("java.security.policy", "C:\\Users\\Usuario\\Documents\\NetBeansProjects\\Multienvio\\src\\multienvio\\server-policy");
        try {
            GroupServer srv = new GroupServer();
            LocateRegistry.createRegistry(1099);
            Registry registry = LocateRegistry.getRegistry(1099);
            registry.rebind("GroupServer", srv);
        } catch (RemoteException ex) {
            Logger.getLogger(GroupServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
