package multienvioOrdenado;

import java.io.Serializable;

/**
 * Mensaje de un grupo
 */
public class GroupMessage implements Serializable {
    byte[] mensaje;
    GroupMember emisor; //aqui ya esta indicado el grupo
    int seqnum;

    public GroupMessage(byte[] mensaje, GroupMember sender, int seqnum) {
        this.mensaje = mensaje;
        emisor = sender;
        this.seqnum = seqnum;
    }
    
    public GroupMember getEmisor() {
        return emisor;
    }

    public int getSeqnum() {
        return seqnum;
    }

    public byte[] getMensaje() {
        return mensaje;
    }
}
