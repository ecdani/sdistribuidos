package multienvioOrdenado;

import java.io.Serializable;

/**
 * Representa a un miembro de un grupo
 * @author usuario
 */
public class GroupMember implements Serializable {
    private String alias, hostname;
    private int idUser, idGroup, puertoreg;

    public String getHostname() {
        return hostname;
    }

    public int getPuertoreg() {
        return puertoreg;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(int idGroup) {
        this.idGroup = idGroup;
    }

    /**
     * Constructor de GroupMember
     * @param alias Un alias identificativo para localizar el miembro rápidamente
     * @param hostname Nombre de host del cliente
     * @param idUser Identificador unico de usuario
     * @param idGroup Identificador unico de grupo
     * @param puertoreg Puerto del cliente
     */
    public GroupMember(String alias, String hostname, int idUser, int idGroup, int puertoreg) {
        this.alias = alias;
        this.hostname = hostname;
        this.idUser = idUser;
        this.idGroup = idGroup;
        this.puertoreg = puertoreg;
    }
}
