package multienvioOrdenado;

import java.nio.charset.StandardCharsets;
import java.rmi.NoSuchObjectException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Cliente ejecutable
 */
public class Client extends UnicastRemoteObject implements ClientInterface {

    int id; // Identificador del grupo
    int puertoreg = 1100;
    GroupServerInterface srv;
    String alias = "", galias = "", ualias = "", hostname = "", ownhost = "";
    public Scanner in;
    public LinkedList<GroupMessage> cola = new LinkedList<>();
    final ReentrantLock lock = new ReentrantLock(true);
    final Condition noHayMensajes = lock.newCondition();
    boolean bloqueado = false;
    HashMap<Integer, HashMap> numbers = new HashMap<>();
    
    /**
     * Constructor de cliente
     * @throws RemoteException 
     */
    public Client() throws RemoteException {
        super();
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        System.out.println("Introduce el alias del cliente:");
        in = new Scanner(System.in);
        alias = in.next();
        System.out.println("Introduce el hostname del server:");
        in = new Scanner(System.in);
        hostname = in.next();
        System.out.println("Introduce tu IP:");
        in = new Scanner(System.in);
        ownhost = in.next();
    }
    
    /**
     * Interfaz para crear grupos
     */
    public void crearGrupo() {
        try {
            System.out.println("Pon nombre de grupo");
            galias = in.next();
            id = srv.createGroup(galias, alias, ownhost, puertoreg);
            if (id != -1) {
                System.out.println("Hemos creado tu grupo, de id: " + id);
                numbers.put(id, new HashMap<Integer, Integer>());
            }
        } catch (RemoteException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Interfaz para eliminar grupos
     */
    public void EliminarGrupo() {
        try {
            System.out.println("Pon nombre del dueño");
            ualias = in.next();
            System.out.println("Borras por nombre(a) o por id(b)?");
            String op = in.next();
            switch (op) {
                case "a":
                    System.out.println("Pon nombre del grupo");
                    galias = in.next();
                    if (srv.removeGroup(galias, ualias)) {
                        //si eliminamos por alias, llamamos a findGroup para que nos devuelva su id, segun el cual se indexa numbers
                        numbers.remove(srv.findGroup(galias));
                        System.out.println("Todo bien");
                    }
                    break;
                case "b":
                    System.out.println("Pon id del grupo");
                    id = in.nextInt();
                    if (srv.removeGroup(id, ualias)) {
                        numbers.remove(id);
                        System.out.println("Todo mal");
                        System.out.println("Todo bien");
                    }
                    break;
            }
        } catch (RemoteException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Interfaz para añadir miembros
     */
    public void AñadirMiembro() {
        try {
            System.out.println("Pon nombre de usuario:");
            ualias = in.next();
            System.out.println("Pon nombre de host:");
            hostname = in.next();
            System.out.println("Pon id de grupo:");
            id = in.nextInt();
            GroupMember nuevo = srv.addMember(id, ualias, hostname, puertoreg);
            if (nuevo != null) {
                updatenumber(nuevo);
                System.out.println("Usuario: " + nuevo.getIdUser() + " de grupo :" + id);
                System.out.println("Todo bien");
            } else {
                System.out.println("Ya existia");
            }
        } catch (RemoteException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Interfaz para eliminar miembros
     */
    public void EliminarMiembro() {
        try {
            System.out.println("Pon nombre de usuario");
            ualias = in.next();
            System.out.println("Pon id de grupo");
            id = in.nextInt();
            //GroupMember aBorrar = srv.isMember(id, ualias);
            if (!srv.removeMember(id, ualias)) {
                System.out.println("Error en el borrado");
            } else {
                //numbers.get(id).remove(aBorrar.getIdUser());
                System.out.println("Todo bien");
            }
        } catch (RemoteException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Desconecta el cliente
     */
    public void Terminar() {
        try {
            UnicastRemoteObject.unexportObject(this, true);
        } catch (NoSuchObjectException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Auxiliar, para depuracion, imprime el numero de grupos
     */
    public void ComprobarNGrupo() {
        try {
            System.out.println(srv.numeroGrupos());
        } catch (RemoteException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Auxiliar, para depuracion, imprime el numero de miembros de un grupo
     */
    public void ComprobarNmiembros() {
        try {
            System.out.println("Alias de grupo");
            System.out.println(srv.numeromiembros(in.next()));
        } catch (RemoteException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Interfaz para enviar un mensaje a tu grupo
     */
    public void EnviarMensaje() {
        GroupMember yo = null;
        System.out.println("Pon id de grupo");
        id = in.nextInt();
        try {
            String mensaje = "";
            System.out.println("Indica tu mensaje");
            mensaje = in.next();
            System.out.println("Mensaje leido, vamos a buscarte");
            yo = srv.isMember(id, alias);
            if (yo == null) {
                System.out.println("No perteneces a ese grupo");
            } else {
                System.out.println("Llamando a servidor, cliente " + alias + " Mensaje: " + mensaje);
                srv.sendGroupMessage(yo, mensaje.getBytes());
            }
        } catch (RemoteException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Josemaria "C:\\Users\\Usuario\\Documents\\NetBeansProjects\\Multienvio\\src\\multienvio\\cliente-policy"
        //AMD "D:\Escritorio\multienvioOrdenado2\\src\\multienvioOrdenado\\cliente-policy"
        // i7 "C:\\Users\\Dani\\Documentas\\git\\sdistribuidos\\multienviOrdenado\\src\\multienvioOrdenado\\cliente-policy"
        //JoseMaria "C:\\Users\\Usuario\\Documents\\cliente-policy"
        System.setProperty("java.security.policy", "C:\\Users\\Usuario\\Documents\\NetBeansProjects\\Multienvio\\src\\multienvio\\cliente-policy");
        try {
            Client c = new Client();
            LocateRegistry.createRegistry(1100);
            Registry registroCliente = LocateRegistry.getRegistry(1100);
            registroCliente.rebind(c.alias, c);
            Registry registroServidor = LocateRegistry.getRegistry(c.hostname, 1099);
            c.srv = (GroupServerInterface) registroServidor.lookup("GroupServer");

            int i = 0;

            System.out.println("Elija una accion:");
            while (i != -1) {
                System.out.println("1. Crear grupo");
                System.out.println("2. Eliminar grupo");
                System.out.println("3. Añadir miembro");
                System.out.println("4. Eliminar miembro");
                System.out.println("5. Envio mensaje de grupo");
                System.out.println("6. Recogida de mensaje de grupo");
                System.out.println("7. Comprobar número de miembros");
                System.out.println("8. Comprobar número de grupo");
                System.out.println("9. Terminar");
                

                String s = c.in.next();
                switch (s) {
                    case "1"://1. Crear grupo
                        c.crearGrupo();
                        break;
                    case "2"://2. Eliminar grupo
                        c.EliminarGrupo();
                        break;
                    case "3"://3. Añadir miembro
                        c.AñadirMiembro();
                        break;
                    case "4"://4. Eliminar miembro
                        c.EliminarMiembro();
                        break;
                    case "5"://5. Enviar mensaje de grupo
                        c.EnviarMensaje();
                        //System.out.println("¡No se puede en esta práctica!");
                        break;
                    case "6": //6. Recibir mensajes de grupo
                        c.recibirMensaje();
                        //System.out.println("¡No se puede en esta práctica!");
                        break;
                    case "8"://8. Comprobar número de grupo
                        c.ComprobarNGrupo();
                        break;
                    case "7":
                        c.ComprobarNmiembros();
                        break;
                    case "9":
                        c.Terminar();
                        i = -1;
                        break;
                }
            }
        } catch (RemoteException ex) {
            Logger.getLogger(GroupServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NotBoundException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Callback para que el servidor pueda dejar mensajes en la cola local del
     * cliente
     *
     * @param m Mensaje desde el servidor
     */
    @Override
    public void DepositMessage(GroupMessage m) {
        lock.lock();
        try {
            cola.add(m);
            //esperaMensajes = true;
            System.out.println("Mensaje encolado y bandera levantada");
            noHayMensajes.signal();
        } finally {
            lock.unlock();
        }
    }

    /**
     * Recuperar mensajes de un grupo de la cola local.
     *
     * @param galias Alias de grupo
     * @return Null o bytes del mensaje
     */
    @Override
    public byte[] receiveGroupMessage(String galias) {
        lock.lock();
        byte[] vacio = new byte[1];
        try {
            if (!bloqueado) {
                int idGr = srv.findGroup(galias);
                if (idGr != -1) {
                    for (GroupMessage m : cola) {
                        if (m.getEmisor().getIdGroup() == idGr) {
                            if (seqnumber(m.getEmisor()) == m.getSeqnum()) {
                                System.out.println("Sacamos mensaje de cola");
                                updatenumber(m.getEmisor());
                                cola.remove(m);
                                return m.getMensaje();
                            } else {
                                //si el mensaje no lleva el numero que estamos esperando, nos bloqueamos
                                bloqueado = true;
                                return vacio;
                            }
                        }
                    }
                } else {
                    //si el grupo no existe en el servidor, aunque los mensajes del mismo esten en nuestra cola, no accedemos a ello
                    return vacio;
                }
                //si recorremos la cola de mensajes pero no hay ninguno del grupo que hemos buscado, nos quedamos a la espera
                noHayMensajes.await();
            } 
        } catch (RemoteException | InterruptedException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            lock.unlock();
        }
        return vacio;
    }

    /**
     * Uso de receibeGroupMessage, Ver mensaje por consola
     */
    public void recibirMensaje() {
        System.out.println("Introduce alias de grupo:");
        String grupo = in.next();
        String men = new String(receiveGroupMessage(grupo), StandardCharsets.UTF_8);
        System.out.println(men);
    }

    /**
     * Devuelve el numero de secuencia esperado
     *
     * @param gm GroupMember
     * @return int Numero de secuencia espera del miembro.
     */
    private int seqnumber(GroupMember gm) {
        System.out.println("Buscando numero de secuencia de "+gm.getIdUser());
        HashMap<Integer, Integer> hm = numbers.get(gm.getIdGroup());
        if (hm != null) {
            Integer theoricalSeqNumber = hm.get(gm.getIdUser());
            if (theoricalSeqNumber != null) {
                System.out.println("Encontrado siguiente numero");
                return (int)theoricalSeqNumber;
            }
        }
        System.out.println("No lo hemos encontrado, asi que 1");
        return 1;
    }

    /**
     * Actualiza el numero de secuencia del miembro del que hemos recibido el
     * mensaje
     *
     * @param gm GroupMember Miembro
     */
    private void updatenumber(GroupMember gm) {
        System.out.println("Usuario: " + gm.getIdUser() + " de grupo :" + gm.getIdGroup());
        HashMap<Integer, Integer> hm = numbers.get(gm.getIdGroup());
        Integer numeroDeSecuencia;
        if (hm != null) {
            numeroDeSecuencia = hm.get(gm.getIdUser());
            if (numeroDeSecuencia != null) {
                hm.put(gm.getIdUser(), ++numeroDeSecuencia);
                System.out.println("Numero actualizado");
            } else {
                numbers.get(gm.getIdGroup()).put(gm.getIdUser(), 1);
                System.out.println("Esta el grupo pero no el user");
            }
        } else {
            numbers.put(gm.getIdGroup(), new HashMap<Integer, Integer>());
            numbers.get(gm.getIdGroup()).put(gm.getIdUser(), 1);
            System.out.println("No estaba ni grupo ni user");

        }
    }
}
