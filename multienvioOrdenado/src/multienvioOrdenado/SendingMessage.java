package multienvioOrdenado;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SendingMessage extends Thread {
    ObjectGroup og;
    GroupMessage m;
    GroupMember emisor;
    ReentrantLock lock = new ReentrantLock(true);

    public SendingMessage(ObjectGroup og, GroupMessage m, GroupMember emisor) {
        this.og = og;
        this.m = m;
        this.emisor = emisor;
    }

    @Override
    public void run() {
        System.out.println("Hilo de envio lanzado.");
        for (GroupMember miembro : og.miembros) {
            if (!miembro.getAlias().equals(emisor.getAlias())) {
                try {
                    System.out.println("Obteniendo stub del cliente.");
                    Registry registroCliente = LocateRegistry.getRegistry(miembro.getHostname(), miembro.getPuertoreg());
                    ClientInterface c = (ClientInterface) registroCliente.lookup(miembro.getAlias());
                    System.out.println("Depositando mensaje en el cliente.");
                    c.DepositMessage(m);
                } catch (RemoteException | NotBoundException ex) {
                    Logger.getLogger(SendingMessage.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        //og.EndSending(emisor);
    }
}
