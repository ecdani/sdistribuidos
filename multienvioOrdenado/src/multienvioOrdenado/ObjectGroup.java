package multienvioOrdenado;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Representa un grupo
 */
public class ObjectGroup {
    int gid = -1;
    String alias = "";
    ArrayList<GroupMember> miembros = new ArrayList<>();
    HashMap<Integer, Integer> Sequential = new HashMap<>();
    GroupMember propietario = null;
    int envios_en_curso = 0;
    int idUserContador = 0;
    boolean stop = false;
    final ReentrantLock mutex = new ReentrantLock(true);
    final ReentrantLock mutex2 = new ReentrantLock(true);
    final Condition control = mutex.newCondition();

    /**
     * Crea un nuevo objeto de grupo cuyo alias e identificador numérico se
     * indica en los argumentos, y cuyo primer miembro es el indicado en los
     * argumentos (alias y hostname). Este último se convierte además en
     * propietario del grupo. En este caso no es necesario el bloqueo de
     * exclusión mutua.
     *
     * @param gid
     * @param galias
     * @param ualias
     * @param uhostname
     * @param puertoreg
     */
    public ObjectGroup(int gid, String galias, String ualias, String uhostname, int puertoreg) {
        this.gid = gid;
        this.alias = galias;
        this.propietario = this.addMember(ualias, uhostname, puertoreg);
        System.out.println("Creado objeto Grupo");
        //this.puertoreg = puertoreg;
    }

    /**
     * Devuleve el propietario del grupo
     *
     * @return GroupMember propietario
     */
    public GroupMember getPropietario() {
        return propietario;
    }

    /**
     * Devuelve identificador de grupo
     *
     * @return int gid
     */
    public int getGid() {
        return gid;
    }

    /**
     * Devuelve el alias de grupo
     *
     * @return String alias
     */
    public String getAlias() {
        return alias;
    }

    /**
     * Se comprueba si un objeto cuyo alias se indica como argumento es miembro
     * del grupo. Si lo es retorna su correspondiente objeto de clase
     * GroupMember, en caso contrario retorna null.
     *
     * @param ualias alias de miembro
     * @return El miembro si existe, sino null
     */
    GroupMember isMember(String ualias) {
        System.out.println("Comprobando si " + ualias + " es miembro en Object group");
        mutex.lock();
        GroupMember miembro = null;
        Iterator it = miembros.iterator();
        while (it.hasNext()) {
            miembro = (GroupMember) it.next();
            if (miembro.getAlias().equals(ualias)) {
                System.out.println("Encontrada coincidencia");
                mutex.unlock();
                return miembro;
            }
        }
        mutex.unlock();
        return null;
    }

    /**
     * Se incluye el objeto cuyo alias se indica en los argumentos como nuevo
     * miembro del grupo, salvo que ya existiese en dicho grupo uno con el mismo
     * alias. Al nuevo miembro se le asignar´a un nuevo identificador. Retorna
     * null si ya existe un miembro con el alias indicado. Debe bloquearse al
     * invocador si las inserciones y borrados de sus miembros est´an
     * bloqueadas.
     *
     * @param alias
     * @param hostname
     * @param puertoreg
     * @return
     */
    public GroupMember addMember(String alias, String hostname, int puertoreg) {
        try {
            mutex.lock();
            if (stop) {
                System.out.println("Bloqueado");
                control.await();
            }
            GroupMember nuevo = new GroupMember(alias, hostname, idUserContador++, gid, puertoreg);
            //Añadimos a la lista de numeros secuenciales el numero de mensaje que usara este miembro
            //La posicion de su numero en su array coincidirá con su idUsuario
            Sequential.put(nuevo.getIdUser(), 0);
            this.miembros.add(nuevo);
            System.out.println("Añadido a Miembros.");
            mutex.unlock();
            return nuevo;
        } catch (InterruptedException ex) {
            Logger.getLogger(ObjectGroup.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            System.out.println("FIN ADD DESDE FINALLY");
        }
        return null;
    }

    /**
     * Se elimina del grupo al objeto cuyo alias se indica como argumento. No
     * puede eliminarse al propietario del grupo ni un objeto que no es miembro
     * del grupo, retornando false
     *
     * @param alias
     * @return
     */
    public boolean removeMember(String alias) {
        mutex.lock();
        try {
            if (stop) {
                System.out.println("Bloqueado");
                control.await();
            }
            GroupMember miembro = this.isMember(alias);
            if ((miembro != null) && !propietario.getAlias().equals(alias)) {
                Sequential.remove(miembro.getIdUser());
                boolean borrado = miembros.remove(miembro);
                mutex.unlock();
                return borrado;
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(ObjectGroup.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    /**
     * Bloquea la addición de usuarios
     */
    public void StopMembers() {
        mutex.lock();
        try {
            stop = true;
        } finally {
            mutex.unlock();
        }
    }

    /**
     * Permite la adiccion de usuarios
     */
    public void AllowMembers() {
        mutex.lock();
        try {
            stop = false;
            control.signalAll();
        } finally {
            mutex.unlock();
        }
    }

    /**
     * Para controlar el número de envíos en curso, incrementando un contador a
     * tal efecto. Se invoca al iniciar un envío, y permite activar el bloqueo
     * de altas/bajas de miembros del grupo. El mecanismo de bloqueo es similar
     * al utilizado en la práctica anterior. Usamos otro lock ya que de no
     * hacerlo, cogeriamos lock, llamariamos a StopMembers teniendo ya cogido el
     * lock, lo que daría lugar a un interbloqueo
     *
     * En la practica 4, al iniciarse un envio se bloquean las altas para
     * siempre (quitamos EndSending y Allow y Stop siguen deshabilitados en
     * GroupServer);
     */
    public void Sending() {
        mutex2.lock();
        System.out.println("Incrementando envios");
        envios_en_curso++;
        StopMembers();
        System.out.println("Bloqueadas altas/bajas");
        mutex2.unlock();
    }

    /**
     * Devuelve el siguiente numero de secuencia para el mensaje de un miembro
     * dado.
     *
     * @param memberid int Identificador de miembro
     * @return int Siguiente numero
     */
    int nextSeqNumber(int memberid) {
        try {
            mutex.lock();
            Integer sig = Sequential.get(memberid);
            if (sig != null) {
                Sequential.put(memberid, ++sig); //lo deja actualizado
                return sig; //devuelve el numero correcto
            } else {
                return -1;
            }
        } finally {
            mutex.unlock();
        }
    }

}
