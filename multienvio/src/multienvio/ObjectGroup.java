package multienvio;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Representa un grupo
 */
public class ObjectGroup {

    int gid = -1;
    String alias = "";
    ArrayList<GroupMember> miembros = new ArrayList<GroupMember>();
    GroupMember propietario = null;
    int envios_en_curso = 0;
    int idUserContador = 0;
    boolean stop = false;
    final ReentrantLock mutex = new ReentrantLock(true);
    final ReentrantLock mutex2 = new ReentrantLock(true);
    final Condition control = mutex.newCondition();

    /**
     * Crea un nuevo objeto de grupo cuyo alias e identificador numérico se
     * indica en los argumentos, y cuyo primer miembro es el indicado en los
     * argumentos (alias y hostname). Este último se convierte además en
     * propietario del grupo. En este caso no es necesario el bloqueo de
     * exclusión mutua.
     *
     * @param gid
     * @param galias
     * @param ualias
     * @param uhostname
     */
    public ObjectGroup(int gid, String galias, String ualias, String uhostname,int puertoreg) {
        this.gid = gid;
        this.alias = galias;
        this.propietario = this.addMember(ualias, uhostname, puertoreg);
        System.out.println("Creado objeto Grupo");
        //this.puertoreg = puertoreg;
    }

    /**
     * Se comprueba si un objeto cuyo alias se indica como argumento es miembro
     * del grupo. Si lo es retorna su correspondiente objeto de clase
     * GroupMember, en caso contrario retorna null.
     * @param ualias alias de miembro
     * @return El miembro si existe, sino null
     */
    GroupMember isMember(String ualias) {
        System.out.println("Comprobando si " + ualias + " es miembro en Object group");
        mutex.lock();
        GroupMember miembro = null;
        Iterator it = miembros.iterator();
        while (it.hasNext()) {
            miembro = (GroupMember) it.next();
            if (miembro.alias.equals(ualias)) {
                System.out.println("Encontrada coincidencia");
                mutex.unlock();
                return miembro;
            }
        }
        mutex.unlock();
        return null;
    }

    public int getGid() {
        return gid;
    }
        
    public String getAlias() {
        return alias;
    }

    /**
     * Se incluye el objeto cuyo alias se indica en los argumentos como nuevo
     * miembro del grupo, salvo que ya existiese en dicho grupo uno con el mismo
     * alias. Al nuevo miembro se le asignar´a un nuevo identificador. Retorna
     * null si ya existe un miembro con el alias indicado. Debe bloquearse al
     * invocador si las inserciones y borrados de sus miembros est´an
     * bloqueadas.
     *
     * @param alias
     * @param hostname
     * @return
     */
    public GroupMember addMember(String alias, String hostname, int puertoreg) {
        try {
            mutex.lock();
            if (stop) {
                System.out.println("Bloqueado");
                control.await();
            }
                GroupMember nuevo = new GroupMember(alias, hostname, idUserContador++, gid, puertoreg);
                this.miembros.add(nuevo);
                System.out.println("Añadido a Miembros.");
                mutex.unlock();
                return nuevo;
        } catch (InterruptedException ex) {
            Logger.getLogger(ObjectGroup.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Se elimina del grupo al objeto cuyo alias se indica como argumento. No
     * puede eliminarse al propietario del grupo ni un objeto que no es miembro
     * del grupo, retornando false
     *
     * @return
     */
    public boolean removeMember(String alias) {
        mutex.lock();
        try {
            if (stop) {
                System.out.println("Bloqueado");
                control.await();
            }
            GroupMember miembro = this.isMember(alias);
            if ((miembro != null) && !propietario.alias.equals(alias)) {
                boolean borrado = miembros.remove(miembro);
                mutex.unlock();
                return borrado;
            }

        } catch (InterruptedException ex) {
            Logger.getLogger(ObjectGroup.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            System.out.println("Liberando lock en finally");
            mutex.unlock();
        }
        return false;
    }

    public GroupMember getPropietario() {
        return propietario;
    }
    
    public void StopMembers() {
        mutex.lock();
        try {
            stop = true;
        } finally {
            mutex.unlock();
        }
    }

    public void AllowMembers() {
        mutex.lock();
        try{
        stop = false;
        control.signalAll();
        } finally {
            mutex.unlock();
        }
    }
    
    /**
     * Para controlar el número de envíos en curso, incrementando un contador 
     * a tal efecto. Se invoca al iniciar un envío, y permite activar el 
     * bloqueo de altas/bajas de miembros del grupo. El mecanismo de bloqueo es 
     * similar al utilizado en la práctica anterior. Usamos otro lock ya que de no hacerlo, cogeriamos 
     *  lock, llamariamos a StopMembers teniendo ya cogido el lock, lo que daría lugar a un interbloqueo
     */
    public void Sending() {
        mutex2.lock();
        System.out.println("Incrementando envios");
        envios_en_curso++;
        StopMembers();
        System.out.println("Bloqueadas altas/bajas");
        mutex2.unlock();
    }
    
    /**
     * Avisa del final de un envío por parte de gm a su grupo, para desbloquear
     * las altas y bajas de miembros del grupo si no hay otros envíos en curso.
     * @param gm GroupMember
     */
    public void EndSending(GroupMember gm) {
        mutex2.lock();
        System.out.println("Decrementando envios");
        envios_en_curso--;
        if (envios_en_curso == 0) {
            AllowMembers();
            System.out.println("Habilitando altas/bajas");
        }
        mutex2.unlock();
    }
}
