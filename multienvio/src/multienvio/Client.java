
package multienvio;

import java.nio.charset.StandardCharsets;
import java.rmi.NoSuchObjectException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Cliente ejecutable
 */
public class Client extends UnicastRemoteObject implements ClientInterface {

    int id; // Identificador del grupo
    int puertoreg = 1100;
    GroupServerInterface srv;
    String alias = "", galias = "", ualias = "", hostname = "", ownhost = "";
    public Scanner in;
    public LinkedList<GroupMessage> cola = new LinkedList<GroupMessage>();
    final ReentrantLock lock = new ReentrantLock(true);
    final Condition control = lock.newCondition();
    boolean esperaMensajes = false;

    public Client() throws RemoteException {
        super();
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        
        System.out.println("Introduce el alias del cliente:");
        in = new Scanner(System.in);
        alias = in.next();
        System.out.println("Introduce el hostname del server (172.19.178.40):");
        in = new Scanner(System.in);
        hostname = in.next();
        System.out.println("Introduce tu hostname  (161.67.196.106):");
        in = new Scanner(System.in);
        ownhost = in.next();
    }

    public void crearGrupo() {
        try {
            System.out.println("Pon nombre de grupo");
            galias = in.next();
            id = srv.createGroup(galias, alias, ownhost, puertoreg);
            if (id != -1) {
                System.out.println("Hemos creado tu grupo, de id: " + id);
            }
        } catch (RemoteException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void EliminarGrupo() {
        try {
            System.out.println("Pon nombre del dueño");
            ualias = in.next();
            System.out.println("Borras por nombre(a) o por id(b)?");
            String op = in.next();
            switch (op) {
                case "a":
                    System.out.println("Pon nombre del grupo");
                    galias = in.next();
                    if (srv.removeGroup(galias, ualias)) {
                        System.out.println("Todo bien");
                    }
                    break;
                case "b":
                    System.out.println("Pon id del grupo");
                    id = in.nextInt();
                    if (srv.removeGroup(id, ualias)) {
                        System.out.println("Todo bien");
                    }
                    break;

            }
        } catch (RemoteException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void AñadirMiembro() {
        try {
            System.out.println("Pon nombre de usuario");
            ualias = in.next();
            System.out.println("Pon nombre de host");
            hostname = in.next();
            System.out.println("Pon id de grupo");
            id = in.nextInt();
            if (srv.addMember(id, ualias, hostname, puertoreg) != null) {
                System.out.println("Todo bien");
            } else {
                System.out.println("Ya existia");
            }
        } catch (RemoteException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void EliminarMiembro() {
        try {
            System.out.println("Pon nombre de usuario");
            ualias = in.next();
            System.out.println("Pon id de grupo");
            id = in.nextInt();
            if (!srv.removeMember(id, ualias)) {
                System.out.println("Ya existía");
            } else {
                System.out.println("Todo bien");
            }
        } catch (RemoteException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*
     public void BloquearAltasBajas() {
     try {
     System.out.println("Pon id de grupo");
     id = in.nextInt();
     srv.StopMembers(id);
     } catch (RemoteException ex) {
     Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
     }
     }

     public void DesbloquearAltasBajas() {
     try {
     System.out.println("Pon id de grupo");
     id = in.nextInt();
     srv.AllowMembers(id);
     } catch (RemoteException ex) {
     Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
     }
     }*/
    public void Terminar() {
        try {
            UnicastRemoteObject.unexportObject(this, true);
        } catch (NoSuchObjectException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void ComprobarNGrupo() {
        try {
            System.out.println(srv.numeroGrupos());
        } catch (RemoteException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void ComprobarNmiembros() {
        try {
            System.out.println("Alias de grupo");
            System.out.println(srv.numeromiembros(in.next()));
        } catch (RemoteException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void EnviarMensaje() {
        GroupMember yo = null;
        System.out.println("Pon id de grupo");
        id = in.nextInt();
        try {
            String mensaje = "";
            System.out.println("Indica tu mensaje");
            mensaje = in.next();
            System.out.println("Mensaje leido, vamos a buscarte");
            yo = srv.isMember(id, alias);
            if (yo == null) {
                System.out.println("No perteneces a ese grupo");
            } else {
            System.out.println("Llamando a servidor, cliente "+alias+" Mensaje: "+mensaje);
                srv.sendGroupMessage(yo, mensaje.getBytes());
            }
        } catch (RemoteException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //"C:\\Users\\Usuario\\Documents\\NetBeansProjects\\Multienvio\\src\\multienvio\\cliente-policy"
        //"D:\\Documentos\\Git\\sdistribuidos\\Multienvio\\src\\multienvio\\cliente-policy"
        System.setProperty("java.security.policy", "C:\\Users\\Usuario\\Documents\\NetBeansProjects\\Multienvio\\src\\multienvio\\cliente-policy");
        System.setProperty("java.security.policy", "D:\\Documentos\\Git\\sdistribuidos\\Multienvio\\src\\multienvio\\cliente-policy");
        try {
            
            Client c = new Client();
            
            LocateRegistry.createRegistry(1100);
            Registry registroCliente = LocateRegistry.getRegistry(1100);
            registroCliente.rebind(c.alias, c);
            //c.hostname guarda el host del servidor
            Registry registroServidor = LocateRegistry.getRegistry(c.hostname, 1099);
            c.srv = (GroupServerInterface) registroServidor.lookup("GroupServer");

            int i = 0;

            System.out.println("Elija una accion:");
            while (i < 10) {
                System.out.println("1. Crear grupo");
                System.out.println("2. Eliminar grupo");
                System.out.println("3. Añadir miembro");
                System.out.println("4. Eliminar miembro");
                System.out.println("5. Envio mensaje de grupo");
                System.out.println("6. Recogida de mensaje de grupo");
                System.out.println("7. Terminar");
                System.out.println("8. Comprobar número de grupo");
                System.out.println("9. Comprobar número de miembros");

                // in = new Scanner(System.in);
                String s = c.in.next();
                switch (s) {
                    case "1"://1. Crear grupo
                        c.crearGrupo();
                        break;
                    case "2"://2. Eliminar grupo
                        c.EliminarGrupo();
                        break;
                    case "3"://3. Añadir miembro
                        c.AñadirMiembro();
                        break;
                    case "4"://4. Eliminar miembro
                        c.EliminarMiembro();
                        break;
                    case "5"://5. Enviar mensaje de grupo
                        c.EnviarMensaje();
                        //System.out.println("¡No se puede en esta práctica!");
                        break;
                    case "6": //6. Recibir mensajes de grupo
                        c.recibirMensaje();
                        //System.out.println("¡No se puede en esta práctica!");
                        break;
                    case "7":
                        c.Terminar();
                        i = 10;
                        break;
                    case "8"://8. Comprobar número de grupo
                        c.ComprobarNGrupo();
                        break;
                    case "9":
                        c.ComprobarNmiembros();
                        break;
                }
                i++;
            }
        } catch (RemoteException ex) {
            Logger.getLogger(GroupServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NotBoundException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Callback para que el servidor pueda dejar mensajes en la cola
     * local del cliente
     * @param m Mensaje desde el servidor
     */
    @Override
    public void DepositMessage(GroupMessage m) {
        lock.lock();
        cola.add(m);
        esperaMensajes = true;
        System.out.println("Mensaje encolado y bandera levantada");
        control.signal();
        lock.unlock();
    }
    
    /**
     * Recuperar mensajes de un grupo de la cola local.
     * @param galias Alias de grupo
     * @return Null o bytes del mensaje
     */
    @Override
    public byte[] receiveGroupMessage(String galias) {
        lock.lock();
        try {
            if (esperaMensajes) {
                int idGr = srv.findGroup(galias);
                if (idGr != -1) {
                    for (GroupMessage m : cola) {
                        if (m.emisor.idGroup == idGr) {
                                System.out.println("Sacamos mensaje de cola");
                                cola.remove(m);
                                return m.getMensaje();
                        }
                    }
                } else return null;
                esperaMensajes = false;
            } else control.await();
        } catch (RemoteException | InterruptedException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            lock.unlock();
        }
        return null;
    }
    
    /**
     * Uso de receibeGroupMessage, Ver mensaje por consola
     */
    public void recibirMensaje() {
        System.out.println("Introduce alias de grupo:");
        String grupo = in.next();
        String men = new String(receiveGroupMessage(grupo), StandardCharsets.UTF_8);
        System.out.println(men);
    }
}
