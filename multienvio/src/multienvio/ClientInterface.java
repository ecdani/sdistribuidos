package multienvio;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ClientInterface extends Remote  {

    /**
     * Es un callback (método invocado desde el servidor), para depositar el 
     * mensaje m en la cola local del cliente. Como puede haber varios objetos 
     * enviando mensajes a la vez al cliente, éste debe garantizar la exclusión
     * mutua en dichas operaciones. Para ello se utilizaría un Lock 
     * (implementación: ReentrantLock).
     * @param m mensaje
     * @throws java.rmi.RemoteException
     */
    void DepositMessage(GroupMessage m) throws RemoteException;
    
    /**
     * Para recoger de su cola local el siguiente mensaje correspondiente al 
     * grupo de alias indicado. Si no hay ninguno se bloquea hasta que llegue
     * uno. Si no existe ese grupo retorna null.
     * @param galias
     * @return 
     * @throws java.rmi.RemoteException 
     */
    byte[] receiveGroupMessage(String galias) throws RemoteException;
}
