package multienvio;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface GroupServerInterface extends Remote{

    /**
     * Para crear un nuevo grupo, con identificador textual galias. El
     * propietario del grupo es su creador, con alias oalias, ubicado en
     * ohostname. Se retorna el identificador num´erico del nuevo grupo en el
     * servidor (mayor o igual a 0) o -1 en caso de error (por ejemplo, ya
     * existe un grupo con ese alias).
     *
     * @param galias
     * @param oalias
     * @param ohostname
     * @return
     */
    int createGroup(String galias, String oalias, String ohostname, int puertoreg) throws RemoteException;

    /**
     * Para localizar un grupo por su identificador textual. Retorna su
     * identificador o -1 si no existe.
     * Podria ser private, pero no.
     * @param galias
     * @return
     */
    int findGroup(String galias) throws RemoteException;

    /**
     * Para eliminar el grupo con identi- ficador textual galias, si existe y su
     * propietario es el objeto con identificador textual oalias. Retorna true
     * si pudo borrarse, false en caso contrario.
     *
     * @param galias
     * @param oalias
     * @return
     */
    boolean removeGroup(String galias, String oalias) throws RemoteException;

    /**
     * Para eliminar el grupo con identificador num´erico gid, si existe y su
     * propietario es el objeto con identificador textual oalias. Retorna true
     * si pudo borrarse, false en caso contrario.
     *
     * @param gid
     * @param oalias
     * @return
     */
    boolean removeGroup(int gid, String oalias) throws RemoteException;

    /**
     * Para añadir como nuevo miembro del grupo al objeto con el alias
     * indicado, que está ubicado en hostname. Retorna null si ya existe.
     *
     * @param gid
     * @param alias
     * @param hostname
     * @return
     */
    GroupMember addMember(int gid, String alias, String hostname, int puertoreg) throws RemoteException;
    
    /**
     * Elimina al usuario con alias "alias" del grupo con id "gid"
     * 
     * @param gid
     * @param alias
     * @return 
     */
    boolean removeMember(int gid, String alias) throws RemoteException;
    
    /**
     * Obtiene un miembro del grupo gid por su alias como miembro del grupo.
     * Retorna null si no existe.
     *
     * @param gid
     * @param alias
     * @return
     */
    GroupMember isMember(int gid, String alias) throws RemoteException;

    /**
     * Se bloquean los intentos de anadir/eliminar miembros del grupo. Devuelve
     * false si no existe ese grupo
     *
     * @param gid
     * @return
     */
    //boolean StopMembers(int gid) throws RemoteException;

    /**
     * Para permitir de nuevo las inserciones y borrados de miembros del grupo.
     * Devuelve false si no existe ese grupo.
     *
     * @param gid
     * @return
     */
    //boolean AllowMembers(int gid) throws RemoteException;
    
    /**
     * Multienvío del mensaje msg por parte de gm, al grupo al que pertenece
     * gm. Mientras hay envíos en curso en un grupo no pueden darse de alta 
     * o baja miembros en el mismo.
     * @param gm Un GroupMember
     * @param msg Un mensaje en secuencia de bytes.
     * @return Retorna true al terminar de enviar el mensaje a todos los miembros del grupo.
     */
    public boolean sendGroupMessage(GroupMember gm, byte[] msg) throws RemoteException;
    
    /**
     * Metodo auxiliar para depuracion, devuelve el número de grupos
     * existente en el servidor.
     * @return int Numero de grupos.
     * @throws RemoteException 
     */
    int numeroGrupos() throws RemoteException;
    
    /**
     * Metodo auxiliar para depuración, devuelve el número de
     * miembros de un grupo con un alias dado.
     * @param alias Alias para localizar el grupo
     * @return int El numero de miembros en el grupo
     * @throws RemoteException 
     */
    public int numeromiembros(String alias)throws RemoteException;
}
