package multienvio;

import java.io.Serializable;

/**
 * Representa a un miembro de un grupo
 * @author usuario
 */
public class GroupMember implements Serializable {
    String alias, hostname;
    int idUser, idGroup, puertoreg;

    /**
     * Constructor de GroupMember
     * @param alias Un alias identificativo para localizar el miembro rápidamente
     * @param hostname Nombre de host del cliente
     * @param idUser Identificador unico de usuario
     * @param idGroup Identificador unico de grupo
     * @param puertoreg Puerto del cliente
     */
    public GroupMember(String alias, String hostname, int idUser, int idGroup, int puertoreg) {
        this.alias = alias;
        this.hostname = hostname;
        this.idUser = idUser;
        this.idGroup = idGroup;
        this.puertoreg = puertoreg;
    }
}
