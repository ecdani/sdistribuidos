package multienvio;

import java.io.Serializable;

/**
 * Mensaje de un grupo
 */
public class GroupMessage implements Serializable {
    byte[] mensaje;
    GroupMember emisor; //aqui ya esta indicado el grupo

    public GroupMessage(byte[] mensaje, GroupMember sender) {
        this.mensaje = mensaje;
        emisor = sender;
    }

    public byte[] getMensaje() {
        return mensaje;
    }
}
